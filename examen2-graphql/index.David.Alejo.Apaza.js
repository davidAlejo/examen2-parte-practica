const { ApolloServer, gql } = require('apollo-server')
const fetch = require('node-fetch');

const typeDefs = gql`

  type Entidad {
    "Obtener todas las entidades"
      id: ID
      name: String
      number: String
  }

  type Mutation {
    "Agrega un nuevo Personaje"
    addEntity(
      id: ID
      name: String!
      number: String!
    ): Entidad
  }

  type Query {
    entidad(id: ID!): Entidad
    entidades: [Entidad]
    entidadCount: Int
  }
`

const resolvers = {
  Query: {
    entidades: () => {
      const asdf = fetchsEntidades()
      console.log(asdf)
      return asdf
    },
    entidad: (parent, args) => {
        const { id } = args
        return fetchEntidad({id})
    },
    entidadCount: async(parent, args) => {
      const personajes = await fetchsEntidades()
      return personajes.length
    }
  },
  /*Mutation: {
    addEntity: async(root, args) => {
      let entitys = await fetchsCharacters()
      if (!entitys.find((entity) => entity.name === args.name)) {
        const newEntity = {
          id: uuid(),
          name: args.name,
          number:args.number
        }
        entitys = entitys.concat(newEntity)
        return newEntity
      }
    },
  }*/
}

const fetchEntidad = ({ id }) =>{
    return fetch(`https://rickandmortyapi.com/api/character/${id}`)
        .then(res => res.json())
}

const fetchsEntidades = () =>{
    fetch('http://localhost:3000/api/entidades/')
    .then(res => res.json())
    .then(json => json.results)
}

const server = new ApolloServer({
  typeDefs,
  resolvers,
})

server.listen().then(({ url }) => {
  console.log(`Server ready at ${url}`)
})
