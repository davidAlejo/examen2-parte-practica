import express from 'express'
import { entidades } from '../utils/mockup'
import { crearEntidad, eliminarEntidad, obtenerEntidad, listarEntidades } from '../components/entidad/controller'
import { crearCuenta, eliminarCuenta, obtenerCuenta, listarCuentas } from '../components/cuenta/controller'
import { crearTransaccion, eliminarTransaccion, obtenerTransaccion, listarTransacciones } from '../components/transaccion/controller'

const router = express.Router()

router.get("/", (request, response) => {
    response.send("<h1>Hello World!</h1>");
});

router.get("/info", (request, response) => {
    var cantidad = entidades.length;
    const fecha = new Date();
    fecha.toDateString()

    response.send("<p>Phone has info for " + cantidad + " people</p> <p>" + fecha + "</p>" );
});

/* API ENTIDADES */
router.get("/api/entidades", listarEntidades)

router.get("/api/entidades/:id", obtenerEntidad)

router.delete("/api/entidades/delete/:id", eliminarEntidad)

router.post("/api/entidades", crearEntidad);


/* API CUENTAS */
router.get("/api/cuentas", listarCuentas)

router.get("/api/cuentas/:id", obtenerCuenta)

router.delete("/api/cuentas/delete/:id", eliminarCuenta)

router.post("/api/cuentas", crearCuenta);


/* API TRANSACCIONES */
router.get("/api/entidades", listarTransacciones)

router.get("/api/entidades/:id", obtenerTransaccion)

router.delete("/api/entidades/delete/:id", eliminarTransaccion)

router.post("/api/entidades", crearTransaccion);

export default router