/**
 * @param {Object} obj
 * @param {import('../infraestructure/MongoCuentaRepository')} obj.CuentaRepository
 */

 export default ({ CuentaRepository }) => {
    return async ({ nroCuenta, saldo, nombreCuenta, token }) => {
      const newAccount = {
        nroCuenta: nroCuenta,
        saldo: saldo,
        nombreCuenta: nombreCuenta,
        token: token
      }
      return await CuentaRepository.add(newAccount)
    }
  }