import MongoLib from '../../../lib/mongo'

class MongoCuentaRepository {
  constructor () {
    this.collection = 'Cuenta'
    this.mongoDB = new MongoLib()
  }

  async add (cuenta) {
    const id = await this.mongoDB.create(this.collection, cuenta)
    return { id, ...cuenta }
  }

  /*   async update ({ id, user }) {
    return this.mongoDB.update(this.collection, id, user)
  } */

  async delete ({ id }) {
    return this.mongoDB.delete(this.collection, id)
  }

  async getById ({ id }) {
    return await this.mongoDB.get(this.collection, id)
  }

  async getAll () {
    const query = null
    return this.mongoDB.getAll(this.collection, query)
  }
}

export default MongoCuentaRepository
