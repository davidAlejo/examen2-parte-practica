import MongoCuentaRepository from './infraestructure/MongoCuentaRepository'
import CrearCuenta from './application/crearCuenta'
import EliminarCuenta from './application/eliminarCuenta'
import ObtenerCuenta from './application/obtenerCuenta'
import ListarCuentas from './application/listarCuentas'
// const ucSignUpBasic = require('./application/signUpBasic')

const CuentaRepository = new MongoCuentaRepository()

export const crearCuenta = async (req, res, next) => {
  try {
    const query = CrearCuenta({ CuentaRepository })
    const cuenta = await query(req.body)
    res.status(201).json({
      data: cuenta,
      message: 'Cuenta Creada Exitosamente'
    })
  } catch (e) {
    next(e)
  }
}

export const eliminarCuenta = async (req, res, next) => {
  try {
    const id = Number(req.params.id)
    const query = EliminarCuenta({ CuentaRepository })
    await query(id)
    res.status(204).json({
      message: 'Cuenta Eliminada Exitosamente'
    })
  } catch (e) {
    next(e)
  }
}

export const obtenerCuenta = async (req, res, next) => {
  try {
    const id = Number(req.params.id)
    const query = ObtenerCuenta({ CuentaRepository })
    const cuenta = await query(id)
    res.status(200).json({ cuenta: cuenta })
  } catch (e) {
    next(e)
  }
}

export const listarCuentas = async (_, res, next) => {
  try {
    const query = ListarCuentas({ CuentaRepository })
    const cuentas = await query()
    res.status(200).json({
      data: cuentas
    })
  } catch (e) {
    next(e)
  }
}
