import MongoEntidadRepository from './infraestructure/MongoEntidadRepository'
import CrearEntidad from './application/crearEntidad'
import EliminarEntidad from './application/eliminarEntidad'
import ObtenerEntidad from './application/obtenerEntidad'
import ListarEntidades from './application/listarEntidades'
// const ucSignUpBasic = require('./application/signUpBasic')

const EntityRepository = new MongoEntidadRepository()

export const crearEntidad = async (req, res, next) => {
  try {
    const query = CrearEntidad({ EntityRepository })
    const entidad = await query(req.body)
    res.status(201).json({
      data: entidad,
      message: 'Entidad Creada Exitosamente'
    })
  } catch (e) {
    next(e)
  }
}
export const eliminarEntidad = async (req, res, next) => {
  try {
    const id = Number(req.params.id)
    const query = EliminarEntidad({ EntityRepository })
    await query(id)
    res.status(204).json({
      message: 'Persona Eliminada Exitosamente'
    })
  } catch (e) {
    next(e)
  }
}

export const obtenerEntidad = async (req, res, next) => {
  try {
    const id = Number(req.params.id)
    const query = ObtenerEntidad({ EntityRepository })
    const entidad = await query(id)
    res.status(200).json({ entidad: entidad })
  } catch (e) {
    next(e)
  }
}

export const listarEntidades = async (_, res, next) => {
  try {
    const query = ListarEntidades({ EntityRepository })
    const entidades = await query()
    res.status(200).json({
      data: entidades
    })
  } catch (e) {
    next(e)
  }
}
