/**
 * @param {Object} obj
 * @param {import('../infraestructure/MongoEntidadRepository')} obj.EntityRepository
 */

 export default ({ EntityRepository }) => {
    return async () => {
      return await EntityRepository.getAll()
    }
  }
  