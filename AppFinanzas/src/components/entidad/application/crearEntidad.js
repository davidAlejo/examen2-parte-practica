/**
 * @param {Object} obj
 * @param {import('../infraestructure/MongoEntidadRepository')} obj.EntityRepository
 */

 export default ({ EntityRepository }) => {
    return async ({ name, number }) => {
      const newEntity = {
        name: name,
        number: number
      }
  
      return await EntityRepository.add(newEntity)
    }
  }