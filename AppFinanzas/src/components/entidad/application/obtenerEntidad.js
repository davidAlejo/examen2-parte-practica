/**
 * @param {Object} obj
 * @param {import('../infraestructure/MongoEntidadRepository')} obj.EntityRepository
 */

 export default ({ EntityRepository }) => {
    return async ({ id }) => {
      return await EntityRepository.getById(id)
    }
  }
  