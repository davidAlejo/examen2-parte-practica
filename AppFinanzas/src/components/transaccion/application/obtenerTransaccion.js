/**
 * @param {Object} obj
 * @param {import('../infraestructure/MongoTransaccionRepository')} obj.TransaccionRepository
 */

 export default ({ TransaccionRepository }) => {
    return async ({ id }) => {
      return await TransaccionRepository.getById(id)
    }
  }
  