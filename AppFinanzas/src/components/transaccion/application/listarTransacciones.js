/**
 * @param {Object} obj
 * @param {import('../infraestructure/MongoTransaccionRepository')} obj.TransaccionRepository
 */

 export default ({ TransaccionRepository }) => {
    return async () => {
      return await TransaccionRepository.getAll()
    }
  }
  