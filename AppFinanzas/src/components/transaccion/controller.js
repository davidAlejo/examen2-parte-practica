import MongoTransaccionRepository from './infraestructure/MongoTransaccionRepository'
import CrearTransaccion from './application/crearTransaccion'
import EliminarTransaccion from './application/eliminarTransaccion'
import ObtenerTransaccion from './application/obtenerTransaccion'
import ListarTransacciones from './application/listarTransacciones'
// const ucSignUpBasic = require('./application/signUpBasic')

const TransaccionRepository = new MongoTransaccionRepository()

export const crearTransaccion = async (req, res, next) => {
  try {
    const query = CrearTransaccion({ TransaccionRepository })
    const transaccion = await query(req.body)
    res.status(201).json({
      data: transaccion,
      message: 'Transaccion Realizada Exitosamente'
    })
  } catch (e) {
    next(e)
  }
}
export const eliminarTransaccion = async (req, res, next) => {
  try {
    const id = Number(req.params.id)
    const query = EliminarTransaccion({ TransaccionRepository })
    await query(id)
    res.status(204).json({
      message: 'Transaccion Eliminada Exitosamente'
    })
  } catch (e) {
    next(e)
  }
}

export const obtenerTransaccion = async (req, res, next) => {
  try {
    const id = Number(req.params.id)
    const query = ObtenerTransaccion({ TransaccionRepository })
    const transaccion = await query(id)
    res.status(200).json({ transaccion: transaccion })
  } catch (e) {
    next(e)
  }
}

export const listarTransacciones = async (_, res, next) => {
  try {
    const query = ListarTransacciones({ TransaccionRepository })
    const transacciones = await query()
    res.status(200).json({
      data: transacciones
    })
  } catch (e) {
    next(e)
  }
}
