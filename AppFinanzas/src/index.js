import express from 'express'
import apiFinanzas from './routes/finanzasApi'

const app = express();
app.use(express.json())
app.use('/', apiFinanzas)

app.use((req, res, next) => {
  res.status(404).send('<h1 style="color:red;">La url donde usted trata de ingresar no existe</h1>')
})

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
